<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class Valets.
 *
 * @author  The scaffold-interface created at 2020-08-31 07:40:50pm
 * @link  https://github.com/amranidev/scaffold-interface
 */
class Valets extends Migration
{
    /**
     * Run the migrations.
     *
     * @return  void
     */
    public function up()
    {
        Schema::create('valets',function (Blueprint $table){

        $table->increments('id');
        
        $table->String('Rut');
        
        $table->String('Nombre');
        
        $table->String('Contacto');
        
        /**
         * Foreignkeys section
         */
        
        
        
        // type your addition here

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return  void
     */
    public function down()
    {
        Schema::drop('valets');
    }
}
