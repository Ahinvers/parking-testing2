<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class Parkings.
 *
 * @author  The scaffold-interface created at 2020-08-31 07:49:40pm
 * @link  https://github.com/amranidev/scaffold-interface
 */
class Parkings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return  void
     */
    public function up()
    {
        Schema::create('parkings',function (Blueprint $table){

        $table->increments('id');
        
        $table->String('Numero');
        
        $table->String('Lugar');
        
        $table->String('Fecha');
        
        /**
         * Foreignkeys section
         */
        
        $table->integer('estado_id')->unsigned()->nullable();
        $table->foreign('estado_id')->references('id')->on('estados')->onDelete('cascade');
        
        $table->integer('valet_id')->unsigned()->nullable();
        $table->foreign('valet_id')->references('id')->on('valets')->onDelete('cascade');
        
        $table->integer('vehiculo_id')->unsigned()->nullable();
        $table->foreign('vehiculo_id')->references('id')->on('vehiculos')->onDelete('cascade');
        
        
        
        // type your addition here

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return  void
     */
    public function down()
    {
        Schema::drop('parkings');
    }
}
