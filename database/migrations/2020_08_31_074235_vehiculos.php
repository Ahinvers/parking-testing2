<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class Vehiculos.
 *
 * @author  The scaffold-interface created at 2020-08-31 07:42:35pm
 * @link  https://github.com/amranidev/scaffold-interface
 */
class Vehiculos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return  void
     */
    public function up()
    {
        Schema::create('vehiculos',function (Blueprint $table){

        $table->increments('id');
        
        $table->String('Patente');
        
        $table->String('Marca');
        
        $table->String('Modelo');
        
        $table->String('Comentario');
        
        $table->String('Nombre');
        
        $table->String('Contacto');
        
        /**
         * Foreignkeys section
         */
        
        
        
        // type your addition here

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return  void
     */
    public function down()
    {
        Schema::drop('vehiculos');
    }
}
