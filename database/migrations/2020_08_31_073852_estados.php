<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class Estados.
 *
 * @author  The scaffold-interface created at 2020-08-31 07:38:55pm
 * @link  https://github.com/amranidev/scaffold-interface
 */
class Estados extends Migration
{
    /**
     * Run the migrations.
     *
     * @return  void
     */
    public function up()
    {
        Schema::create('estados',function (Blueprint $table){

        $table->increments('id');
        
        $table->String('Estado');
        
        /**
         * Foreignkeys section
         */
        
        
        
        // type your addition here

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return  void
     */
    public function down()
    {
        Schema::drop('estados');
    }
}
