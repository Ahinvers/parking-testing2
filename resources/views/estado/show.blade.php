@extends('scaffold-interface.layouts.defaultMaterialize')
@section('title','Show')
@section('content')

<div class = 'container'>
    <h1>
        Show estado
    </h1>
    <form method = 'get' action = '{!!url("estado")!!}'>
        <button class = 'btn blue'>estado Index</button>
    </form>
    <table class = 'highlight bordered'>
        <thead>
            <th>Key</th>
            <th>Value</th>
        </thead>
        <tbody>
            <tr>
                <td>
                    <b><i>Estado : </i></b>
                </td>
                <td>{!!$estado->Estado!!}</td>
            </tr>
        </tbody>
    </table>
</div>
@endsection