@extends('scaffold-interface.layouts.defaultMaterialize')
@section('title','Create')
@section('content')

<div class = 'container'>
    <h1>
        Create estado
    </h1>
    <form method = 'get' action = '{!!url("estado")!!}'>
        <button class = 'btn blue'>estado Index</button>
    </form>
    <br>
    <form method = 'POST' action = '{!!url("estado")!!}'>
        <input type = 'hidden' name = '_token' value = '{{ Session::token() }}'>
        <div class="input-field col s6">
            <input id="Estado" name = "Estado" type="text" class="validate">
            <label for="Estado">Estado</label>
        </div>
        <button class = 'btn red' type ='submit'>Create</button>
    </form>
</div>
@endsection