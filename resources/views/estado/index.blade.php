@extends('scaffold-interface.layouts.defaultMaterialize')
@section('title','Index')
@section('content')

<div class = 'container'>
    <h1>
        estado Index
    </h1>
    <div class="row">
        <form class = 'col s3' method = 'get' action = '{!!url("estado")!!}/create'>
            <button class = 'btn red' type = 'submit'>Create New estado</button>
        </form>
    </div>
    <table>
        <thead>
            <th>Estado</th>
            <th>actions</th>
        </thead>
        <tbody>
            @foreach($estados as $estado) 
            <tr>
                <td>{!!$estado->Estado!!}</td>
                <td>
                    <div class = 'row'>
                        <a href = '#modal1' class = 'delete btn-floating modal-trigger red' data-link = "/estado/{!!$estado->id!!}/deleteMsg" ><i class = 'material-icons'>delete</i></a>
                        <a href = '#' class = 'viewEdit btn-floating blue' data-link = '/estado/{!!$estado->id!!}/edit'><i class = 'material-icons'>edit</i></a>
                        <a href = '#' class = 'viewShow btn-floating orange' data-link = '/estado/{!!$estado->id!!}'><i class = 'material-icons'>info</i></a>
                    </div>
                </td>
            </tr>
            @endforeach 
        </tbody>
    </table>
    {!! $estados->render() !!}

</div>
@endsection