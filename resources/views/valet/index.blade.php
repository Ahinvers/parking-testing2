@extends('scaffold-interface.layouts.defaultMaterialize')
@section('title','Index')
@section('content')

<div class = 'container'>
    <h1>
        Listado Valet
    </h1>
    <div class="row">
        <form class = 'col s3' method = 'get' action = '{!!url("valet")!!}/create'>
            <button class = 'btn red' type = 'submit'>Resgistar Valet</button>
        </form>
    </div>
    <table>
        <thead>
            <th>Rut</th>
            <th>Nombre</th>
            <th>Contacto</th>
            <th>actions</th>
        </thead>
        <tbody>
            @foreach($valets as $valet)
            <tr>
                <td>{!!$valet->Rut!!}</td>
                <td>{!!$valet->Nombre!!}</td>
                <td>{!!$valet->Contacto!!}</td>
                <td>
                    <div class = 'row'>
                        <a href = '#modal1' class = 'delete btn-floating modal-trigger red' data-link = "/valet/{!!$valet->id!!}/deleteMsg" ><i class = 'material-icons'>delete</i></a>
                        <a href = '#' class = 'viewEdit btn-floating blue' data-link = '/valet/{!!$valet->id!!}/edit'><i class = 'material-icons'>edit</i></a>
                        <a href = '#' class = 'viewShow btn-floating orange' data-link = '/valet/{!!$valet->id!!}'><i class = 'material-icons'>info</i></a>
                    </div>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    {!! $valets->render() !!}

</div>
@endsection
