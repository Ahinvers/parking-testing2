@extends('scaffold-interface.layouts.defaultMaterialize')
@section('title','Create')
@section('content')

<div class = 'container'>
    <h1>
        Registar Valet
    </h1>
    <form method = 'get' action = '{!!url("valet")!!}'>
        <button class = 'btn blue'>Listado Valet </button>
    </form>
    <br>
    <form method = 'POST' action = '{!!url("valet")!!}'>
        <input type = 'hidden' name = '_token' value = '{{ Session::token() }}'>
        <div class="input-field col s6">
            <input id="Rut" name = "Rut" type="text" class="validate">
            <label for="Rut">Rut</label>
        </div>
        <div class="input-field col s6">
            <input id="Nombre" name = "Nombre" type="text" class="validate">
            <label for="Nombre">Nombre</label>
        </div>
        <div class="input-field col s6">
            <input id="Contacto" name = "Contacto" type="text" class="validate">
            <label for="Contacto">Contacto</label>
        </div>
        <button class = 'btn red' type ='submit'>Create</button>
    </form>
</div>
@endsection
