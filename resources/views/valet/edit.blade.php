@extends('scaffold-interface.layouts.defaultMaterialize')
@section('title','Edit')
@section('content')

<div class = 'container'>
    <h1>
        Actualizar Valet
    </h1>
    <form method = 'get' action = '{!!url("valet")!!}'>
        <button class = 'btn blue'>Listado Valet</button>
    </form>
    <br>
    <form method = 'POST' action = '{!! url("valet")!!}/{!!$valet->
        id!!}/update'>
        <input type = 'hidden' name = '_token' value = '{{Session::token()}}'>
        <div class="input-field col s6">
            <input id="Rut" name = "Rut" type="text" class="validate" value="{!!$valet->
            Rut!!}">
            <label for="Rut">Rut</label>
        </div>
        <div class="input-field col s6">
            <input id="Nombre" name = "Nombre" type="text" class="validate" value="{!!$valet->
            Nombre!!}">
            <label for="Nombre">Nombre</label>
        </div>
        <div class="input-field col s6">
            <input id="Contacto" name = "Contacto" type="text" class="validate" value="{!!$valet->
            Contacto!!}">
            <label for="Contacto">Contacto</label>
        </div>
        <button class = 'btn red' type ='submit'>Update</button>
    </form>
</div>
@endsection
