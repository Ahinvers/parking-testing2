@extends('scaffold-interface.layouts.defaultMaterialize')
@section('title','Show')
@section('content')

<div class = 'container'>
    <h1>
        Valet
    </h1>
    <form method = 'get' action = '{!!url("valet")!!}'>
        <button class = 'btn blue'>Listado Valet</button>
    </form>
    <table class = 'highlight bordered'>
        <thead>
            <th>Key</th>
            <th>Value</th>
        </thead>
        <tbody>
            <tr>
                <td>
                    <b><i>Rut : </i></b>
                </td>
                <td>{!!$valet->Rut!!}</td>
            </tr>
            <tr>
                <td>
                    <b><i>Nombre : </i></b>
                </td>
                <td>{!!$valet->Nombre!!}</td>
            </tr>
            <tr>
                <td>
                    <b><i>Contacto : </i></b>
                </td>
                <td>{!!$valet->Contacto!!}</td>
            </tr>
        </tbody>
    </table>
</div>
@endsection
