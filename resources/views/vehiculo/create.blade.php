@extends('scaffold-interface.layouts.defaultMaterialize')
@section('title','Create')
@section('content')

<div class = 'container'>
    <h1>
        Registar vehiculo
    </h1>
    <form method = 'get' action = '{!!url("vehiculo")!!}'>
        <button class = 'btn blue'>Listado de Vehiculos </button>
    </form>
    <br>
    <form method = 'POST' action = '{!!url("vehiculo")!!}'>
        <input type = 'hidden' name = '_token' value = '{{ Session::token() }}'>
        <div class="input-field col s6">
            <input id="Patente" name = "Patente" type="text" class="validate">
            <label for="Patente">Patente</label>
        </div>
        <div class="input-field col s6">
            <input id="Marca" name = "Marca" type="text" class="validate">
            <label for="Marca">Marca</label>
        </div>
        <div class="input-field col s6">
            <input id="Modelo" name = "Modelo" type="text" class="validate">
            <label for="Modelo">Modelo</label>
        </div>
        <div class="input-field col s6">
            <input id="Comentario" name = "Comentario" type="text" class="validate">
            <label for="Comentario">Comentario</label>
        </div>
        <div class="input-field col s6">
            <input id="Nombre" name = "Nombre" type="text" class="validate">
            <label for="Nombre">Nombre</label>
        </div>
        <div class="input-field col s6">
            <input id="Contacto" name = "Contacto" type="text" class="validate">
            <label for="Contacto">Contacto</label>
        </div>
        <button class = 'btn red' type ='submit'>Create</button>
    </form>
</div>
@endsection
