@extends('scaffold-interface.layouts.defaultMaterialize')
@section('title','Index')
@section('content')

<div class = 'container'>
    <h1>
        Listado de Vehiculo
    </h1>
    <div class="row">
        <form class = 'col s3' method = 'get' action = '{!!url("vehiculo")!!}/create'>
            <button class = 'btn red' type = 'submit'>Resgistar Vehiculo</button>
        </form>
        <form class = 'col s3' method = 'get' action = '{!!url("parking")!!}/create'>
            <button class = 'btn red' type = 'submit'>Registar Parking</button>
        </form>
    </div>
    <table>
        <thead>
            <th>Patente</th>
            <th>Marca</th>
            <th>Modelo</th>
            <th>Comentario</th>
            <th>Nombre Cliente </th>
            <th>Contacto Cliente</th>
            <th>actions</th>
        </thead>
        <tbody>
            @foreach($vehiculos as $vehiculo)
            <tr>
                <td>{!!$vehiculo->Patente!!}</td>
                <td>{!!$vehiculo->Marca!!}</td>
                <td>{!!$vehiculo->Modelo!!}</td>
                <td>{!!$vehiculo->Comentario!!}</td>
                <td>{!!$vehiculo->Nombre!!}</td>
                <td>{!!$vehiculo->Contacto!!}</td>
                <td>
                    <div class = 'row'>
                        <a href = '#modal1' class = 'delete btn-floating modal-trigger red' data-link = "/vehiculo/{!!$vehiculo->id!!}/deleteMsg" ><i class = 'material-icons'>delete</i></a>
                        <a href = '#' class = 'viewEdit btn-floating blue' data-link = '/vehiculo/{!!$vehiculo->id!!}/edit'><i class = 'material-icons'>edit</i></a>
                        <a href = '#' class = 'viewShow btn-floating orange' data-link = '/vehiculo/{!!$vehiculo->id!!}'><i class = 'material-icons'>info</i></a>
                    </div>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    {!! $vehiculos->render() !!}

</div>
@endsection
