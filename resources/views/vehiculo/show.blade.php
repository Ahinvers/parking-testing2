@extends('scaffold-interface.layouts.defaultMaterialize')
@section('title','Show')
@section('content')

<div class = 'container'>
    <h1>
        Vehiculo
    </h1>
    <form method = 'get' action = '{!!url("vehiculo")!!}'>
        <button class = 'btn blue'>Listado Vehiculo</button>
    </form>
    <table class = 'highlight bordered'>
        <thead>
            <th>Key</th>
            <th>Value</th>
        </thead>
        <tbody>
            <tr>
                <td>
                    <b><i>Patente : </i></b>
                </td>
                <td>{!!$vehiculo->Patente!!}</td>
            </tr>
            <tr>
                <td>
                    <b><i>Marca : </i></b>
                </td>
                <td>{!!$vehiculo->Marca!!}</td>
            </tr>
            <tr>
                <td>
                    <b><i>Modelo : </i></b>
                </td>
                <td>{!!$vehiculo->Modelo!!}</td>
            </tr>
            <tr>
                <td>
                    <b><i>Comentario : </i></b>
                </td>
                <td>{!!$vehiculo->Comentario!!}</td>
            </tr>
            <tr>
                <td>
                    <b><i>Nombre : </i></b>
                </td>
                <td>{!!$vehiculo->Nombre!!}</td>
            </tr>
            <tr>
                <td>
                    <b><i>Contacto : </i></b>
                </td>
                <td>{!!$vehiculo->Contacto!!}</td>
            </tr>
        </tbody>
    </table>
</div>
@endsection
