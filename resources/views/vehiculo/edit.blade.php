@extends('scaffold-interface.layouts.defaultMaterialize')
@section('title','Edit')
@section('content')

<div class = 'container'>
    <h1>
        Actualizar vehiculo
    </h1>
    <form method = 'get' action = '{!!url("vehiculo")!!}'>
        <button class = 'btn blue'>Listado de Vehiculos</button>
    </form>
    <br>
    <form method = 'POST' action = '{!! url("vehiculo")!!}/{!!$vehiculo->
        id!!}/update'>
        <input type = 'hidden' name = '_token' value = '{{Session::token()}}'>
        <div class="input-field col s6">
            <input id="Patente" name = "Patente" type="text" class="validate" value="{!!$vehiculo->
            Patente!!}">
            <label for="Patente">Patente</label>
        </div>
        <div class="input-field col s6">
            <input id="Marca" name = "Marca" type="text" class="validate" value="{!!$vehiculo->
            Marca!!}">
            <label for="Marca">Marca</label>
        </div>
        <div class="input-field col s6">
            <input id="Modelo" name = "Modelo" type="text" class="validate" value="{!!$vehiculo->
            Modelo!!}">
            <label for="Modelo">Modelo</label>
        </div>
        <div class="input-field col s6">
            <input id="Comentario" name = "Comentario" type="text" class="validate" value="{!!$vehiculo->
            Comentario!!}">
            <label for="Comentario">Comentario</label>
        </div>
        <div class="input-field col s6">
            <input id="Nombre" name = "Nombre" type="text" class="validate" value="{!!$vehiculo->
            Nombre!!}">
            <label for="Nombre">Nombre</label>
        </div>
        <div class="input-field col s6">
            <input id="Contacto" name = "Contacto" type="text" class="validate" value="{!!$vehiculo->
            Contacto!!}">
            <label for="Contacto">Contacto</label>
        </div>
        <button class = 'btn red' type ='submit'>Update</button>
    </form>
</div>
@endsection
