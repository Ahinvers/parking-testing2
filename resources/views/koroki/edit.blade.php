@extends('scaffold-interface.layouts.defaultMaterialize')
@section('title','Edit')
@section('content')

<div class = 'container'>
    <h1>
        Edit parking
    </h1>
    <form method = 'get' action = '{!!url("parking")!!}'>
        <button class = 'btn blue'>parking Index</button>
    </form>
    <br>
    <form method = 'POST' action = '{!! url("parking")!!}/{!!$parking->
        id!!}/update'> 
        <input type = 'hidden' name = '_token' value = '{{Session::token()}}'>
        <div class="input-field col s6">
            <input id="Numero" name = "Numero" type="text" class="validate" value="{!!$parking->
            Numero!!}"> 
            <label for="Numero">Numero</label>
        </div>
        <div class="input-field col s6">
            <input id="Lugar" name = "Lugar" type="text" class="validate" value="{!!$parking->
            Lugar!!}"> 
            <label for="Lugar">Lugar</label>
        </div>
        <div class="input-field col s6">
            <input id="Fecha" name = "Fecha" type="text" class="validate" value="{!!$parking->
            Fecha!!}"> 
            <label for="Fecha">Fecha</label>
        </div>
        <div class="input-field col s12">
            <select name = 'estado_id'>
                @foreach($estados as $key => $value) 
                <option value="{{$key}}">{{$value}}</option>
                @endforeach 
            </select>
            <label>estados Select</label>
        </div>
        <div class="input-field col s12">
            <select name = 'valet_id'>
                @foreach($valets as $key => $value) 
                <option value="{{$key}}">{{$value}}</option>
                @endforeach 
            </select>
            <label>valets Select</label>
        </div>
        <div class="input-field col s12">
            <select name = 'vehiculo_id'>
                @foreach($vehiculos as $key => $value) 
                <option value="{{$key}}">{{$value}}</option>
                @endforeach 
            </select>
            <label>vehiculos Select</label>
        </div>
        <button class = 'btn red' type ='submit'>Update</button>
    </form>
</div>
@endsection