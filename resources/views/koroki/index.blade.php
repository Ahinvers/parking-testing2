@extends('scaffold-interface.layouts.defaultMaterialize')
@section('title','Index')
@section('content')

<div class = 'container'>
    <h1>
       Listado Parking
    </h1>
    <div class="row">
      <form class = 'col s3' method = 'get' action = '{!!url("vehiculo")!!}'>
          <button class = 'btn red' type = 'submit'>Listado de Vehiculos</button>
      </form>
        <form class = 'col s3' method = 'get' action = '{!!url("parking")!!}/create'>
            <button class = 'btn red' type = 'submit'>Registar Parking</button>
        </form>
        <ul id="dropdown" class="dropdown-content">
            <li><a href="http://localhost/tomoko/public/valet">Valet</a></li>
            <li><a href="http://localhost/tomoko/public/vehiculo">Vehiculo</a></li>
        </ul>
        <a class="col s3 btn dropdown-button #1e88e5 blue darken-1" href="#!" data-activates="dropdown">Associate<i class="mdi-navigation-arrow-drop-down right"></i></a>
    </div>
    <table>
        <thead>
            <th>Numero</th>
            <th>Lugar</th>
            <th>Fecha</th>
            <th>Estado</th>
            <th>Rut</th>
            <th>Patente</th>
            <th>Comentario</th>
            <th>Nombre</th>
            <th>Contacto</th>
            <th>actions</th>
        </thead>
        <tbody>
            @foreach($parkings as $parking)
            <tr>
                <td>{!!$parking->Numero!!}</td>
                <td>{!!$parking->Lugar!!}</td>
                <td>{!!$parking->Fecha!!}</td>
                <td>{!!$parking->estado->Estado!!}</td>
                <td>{!!$parking->valet->Rut!!}</td>
                <td>{!!$parking->vehiculo->Patente!!}</td>
                <td>{!!$parking->vehiculo->Comentario!!}</td>
                <td>{!!$parking->vehiculo->Nombre!!}</td>
                <td>{!!$parking->vehiculo->Contacto!!}</td>
                <td>
                    <div class = 'row'>
                      <!--<a href = '#modal1' class = 'delete btn-floating modal-trigger red' data-link = "/parking/{!!$parking->id!!}/deleteMsg" ><i class = 'material-icons'>delete</i></a>-->            
                        <a href = '#' class = 'viewEdit btn-floating blue' data-link = '/parking/{!!$parking->id!!}/edit'><i class = 'material-icons'>edit</i></a>
                        <a href = '#' class = 'viewShow btn-floating orange' data-link = '/parking/{!!$parking->id!!}'><i class = 'material-icons'>info</i></a>
                    </div>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    {!! $parkings->render() !!}

</div>
@endsection
