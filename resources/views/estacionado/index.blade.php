@extends('scaffold-interface.layouts.defaultMaterialize')
@section('title','Index')
@section('content')

<div class = 'container'>
    <h1>
       Listado estacionado
    </h1>
    <div class="row">
      <form class = 'col s3' method = 'get' action = '{!!url("vehiculo")!!}'>
          <button class = 'btn red' type = 'submit'>Listado de Vehiculos</button>
      </form>
        <form class = 'col s3' method = 'get' action = '{!!url("estacionado")!!}/create'>
            <button class = 'btn red' type = 'submit'>Registar estacionado</button>
        </form>
        <ul id="dropdown" class="dropdown-content">
            <li><a href="http://localhost/tomoko/public/valet">Valet</a></li>
            <li><a href="http://localhost/tomoko/public/vehiculo">Vehiculo</a></li>
        </ul>
        <a class="col s3 btn dropdown-button #1e88e5 blue darken-1" href="#!" data-activates="dropdown">Associate<i class="mdi-navigation-arrow-drop-down right"></i></a>
    </div>
    <table>
        <thead>
            <th>Numero</th>
            <th>Lugar</th>
            <th>Fecha</th>
            <th>Estado</th>
            <th>Rut</th>
            <th>Patente</th>
            <th>Comentario</th>
            <th>Nombre</th>
            <th>Contacto</th>
            <th>actions</th>
        </thead>
        <tbody>
            @foreach($estacionados as $estacionado)
            @if ($estacionados->estado->Estado == "Estacionado")
            <tr>
                <td>{!!$estacionado->Numero!!}</td>
                <td>{!!$estacionado->Lugar!!}</td>
                <td>{!!$estacionado->Fecha!!}</td>
                <td>{!!$estacionado->estado->Estado!!}</td>
                <td>{!!$estacionado->valet->Rut!!}</td>
                <td>{!!$estacionado->vehiculo->Patente!!}</td>
                <td>{!!$estacionado->vehiculo->Comentario!!}</td>
                <td>{!!$estacionado->vehiculo->Nombre!!}</td>
                <td>{!!$estacionado->vehiculo->Contacto!!}</td>
                <td>
                    <div class = 'row'>
                      <!--<a href = '#modal1' class = 'delete btn-floating modal-trigger red' data-link = "/estacionado/{!!$estacionado->id!!}/deleteMsg" ><i class = 'material-icons'>delete</i></a>-->
                        <a href = '#' class = 'viewEdit btn-floating blue' data-link = '/estacionado/{!!$estacionado->id!!}/edit'><i class = 'material-icons'>edit</i></a>
                        <a href = '#' class = 'viewShow btn-floating orange' data-link = '/estacionado/{!!$estacionado->id!!}'><i class = 'material-icons'>info</i></a>
                    </div>
                </td>
            </tr>
            @endif
            @endforeach
        </tbody>
    </table>
    {!! $estacionados->render() !!}

</div>
@endsection
