@extends('scaffold-interface.layouts.defaultMaterialize')
@section('title','Show')
@section('content')

<div class = 'container'>
    <h1>
        Vista Cliente
    </h1>
    <form method = 'get' action = '{!!url("parking")!!}'>
        <button class = 'btn blue'>parking Index</button>
    </form>
    <table class = 'highlight bordered'>
        <thead>
            <th></th>
            <th><button class = 'btn red'>Solicitar</button></th>
        </thead>
        <tbody>
            <tr>
                <td>
                    <b><i>Numero : </i></b>
                </td>
                <td>{!!$parking->Numero!!}</td>
            </tr>
            <tr>
                <td>
                    <b><i>Lugar : </i></b>
                </td>
                <td>{!!$parking->Lugar!!}</td>
            </tr>
            <tr>
                <td>
                    <b><i>Fecha : </i></b>
                </td>
                <td>{!!$parking->Fecha!!}</td>
            </tr>
            <tr>
                <td>
                    <b>
                        <i>Estado : </i>
                        <b>
                        </td>
                        <td>{!!$parking->estado->Estado!!}</td>
                    </tr>
                    <tr>
                        <td>
                            <b>
                                <i>Rut : </i>
                                <b>
                                </td>
                                <td>{!!$parking->valet->Rut!!}</td>
                            </tr>
                            <tr>
                                <td>
                                    <b>
                                        <i>Nombre Valet : </i>
                                        <b>
                                        </td>
                                        <td>{!!$parking->valet->Nombre!!}</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <b>
                                                <i>Contacto de Valet : </i>
                                                <b>
                                                </td>
                                                <td>{!!$parking->valet->Contacto!!}</td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <b>
                                                        <i>Patente : </i>
                                                        <b>
                                                        </td>
                                                        <td>{!!$parking->vehiculo->Patente!!}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <b>
                                                                <i>Marca : </i>
                                                                <b>
                                                                </td>
                                                                <td>{!!$parking->vehiculo->Marca!!}</td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <b>
                                                                        <i>Modelo : </i>
                                                                        <b>
                                                                        </td>
                                                                        <td>{!!$parking->vehiculo->Modelo!!}</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <b>
                                                                                <i>Comentario : </i>
                                                                                <b>
                                                                                </td>
                                                                                <td>{!!$parking->vehiculo->Comentario!!}</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <b>
                                                                                        <i>Nombre Cliente : </i>
                                                                                        <b>
                                                                                        </td>
                                                                                        <td>{!!$parking->vehiculo->Nombre!!}</td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <b>
                                                                                                <i>Contacto Cliente : </i>
                                                                                                <b>
                                                                                                </td>
                                                                                                <td>{!!$parking->vehiculo->Contacto!!}</td>
                                                                                            </tr>
                                                                                        </tbody>
                                                                                    </table>
                                                                                </div>
                                                                                @endsection
