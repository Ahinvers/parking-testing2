<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

//estado Routes
Route::group(['middleware'=> 'web'],function(){
  Route::resource('estado','\App\Http\Controllers\EstadoController');
  Route::post('estado/{id}/update','\App\Http\Controllers\EstadoController@update');
  Route::get('estado/{id}/delete','\App\Http\Controllers\EstadoController@destroy');
  Route::get('estado/{id}/deleteMsg','\App\Http\Controllers\EstadoController@DeleteMsg');
});

//valet Routes
Route::group(['middleware'=> 'web'],function(){
  Route::resource('valet','\App\Http\Controllers\ValetController');
  Route::post('valet/{id}/update','\App\Http\Controllers\ValetController@update');
  Route::get('valet/{id}/delete','\App\Http\Controllers\ValetController@destroy');
  Route::get('valet/{id}/deleteMsg','\App\Http\Controllers\ValetController@DeleteMsg');
});

//vehiculo Routes
Route::group(['middleware'=> 'web'],function(){
  Route::resource('vehiculo','\App\Http\Controllers\VehiculoController');
  Route::post('vehiculo/{id}/update','\App\Http\Controllers\VehiculoController@update');
  Route::get('vehiculo/{id}/delete','\App\Http\Controllers\VehiculoController@destroy');
  Route::get('vehiculo/{id}/deleteMsg','\App\Http\Controllers\VehiculoController@DeleteMsg');
});

//parking Routes
Route::group(['middleware'=> 'web'],function(){
  Route::resource('parking','\App\Http\Controllers\ParkingController');
  Route::post('parking/{id}/update','\App\Http\Controllers\ParkingController@update');
  Route::get('parking/{id}/delete','\App\Http\Controllers\ParkingController@destroy');
  Route::get('parking/{id}/deleteMsg','\App\Http\Controllers\ParkingController@DeleteMsg');
});
//Estacionado Routes
Route::group(['middleware'=> 'web'],function(){
  Route::resource('estacionado','\App\Http\Controllers\EstacionadoController');
  Route::post('parking/{id}/update','\App\Http\Controllers\ParkingController@update');
  Route::get('parking/{id}/delete','\App\Http\Controllers\ParkingController@destroy');
  Route::get('parking/{id}/deleteMsg','\App\Http\Controllers\ParkingController@DeleteMsg');
});
