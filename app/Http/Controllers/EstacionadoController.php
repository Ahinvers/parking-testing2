<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\App;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\estacionado;
use Amranidev\Ajaxis\Ajaxis;
use URL;

use App\Estado;


use App\Valet;


use App\Vehiculo;


/**
 * Class estacionadoController.
 *
 * @author  The scaffold-interface created at 2020-08-31 07:49:40pm
 * @link  https://github.com/amranidev/scaffold-interface
 */
class EstacionadoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return  \Illuminate\Http\Response
     */
    public function index()
    {
        $title = 'Index - estacionado';
        $estacionados = estacionado::paginate(6);
        return view('estacionado.index',compact('estacionados','title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return  \Illuminate\Http\Response
     */
    public function create()
    {
        $title = 'Create - estacionado';

        $estados = Estado::all()->pluck('Estado','id');

        $valets = Valet::all()->pluck('Rut','id');

        $vehiculos = Vehiculo::all()->pluck('Patente','id');

        return view('estacionado.create',compact('title','estados' , 'valets' , 'vehiculos'  ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param    \Illuminate\Http\Request  $request
     * @return  \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $estacionado = new estacionado();


        $estacionado->Numero = $request->Numero;


        $estacionado->Lugar = $request->Lugar;


        $estacionado->Fecha = $request->Fecha;



        $estacionado->estado_id = $request->estado_id;


        $estacionado->valet_id = $request->valet_id;


        $estacionado->vehiculo_id = $request->vehiculo_id;


        $estacionado->save();

        $pusher = App::make('pusher');

        //default pusher notification.
        //by default channel=test-channel,event=test-event
        //Here is a pusher notification example when you create a new resource in storage.
        //you can modify anything you want or use it wherever.
        $pusher->trigger('test-channel',
                         'test-event',
                        ['message' => 'A new estacionado has been created !!']);

        return redirect('estacionado');
    }

    /**
     * Display the specified resource.
     *
     * @param    \Illuminate\Http\Request  $request
     * @param    int  $id
     * @return  \Illuminate\Http\Response
     */
    public function show($id,Request $request)
    {
        $title = 'Show - estacionado';

        if($request->ajax())
        {
            return URL::to('estacionado/'.$id);
        }

        $estacionado = estacionado::findOrfail($id);
        return view('estacionado.show',compact('title','estacionado'));
    }

    /**
     * Show the form for editing the specified resource.
     * @param    \Illuminate\Http\Request  $request
     * @param    int  $id
     * @return  \Illuminate\Http\Response
     */
    public function edit($id,Request $request)
    {
        $title = 'Edit - estacionado';
        if($request->ajax())
        {
            return URL::to('estacionado/'. $id . '/edit');
        }


        $estados = Estado::all()->pluck('Estado','id');


        $valets = Valet::all()->pluck('Rut','id');


        $vehiculos = Vehiculo::all()->pluck('Patente','id');


        $estacionado = estacionado::findOrfail($id);
        return view('estacionado.edit',compact('title','estacionado' ,'estados', 'valets', 'vehiculos' ) );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param    \Illuminate\Http\Request  $request
     * @param    int  $id
     * @return  \Illuminate\Http\Response
     */
    public function update($id,Request $request)
    {
        $estacionado = estacionado::findOrfail($id);

        $estacionado->Numero = $request->Numero;

        $estacionado->Lugar = $request->Lugar;

        $estacionado->Fecha = $request->Fecha;


        $estacionado->estado_id = $request->estado_id;


        $estacionado->valet_id = $request->valet_id;


        $estacionado->vehiculo_id = $request->vehiculo_id;


        $estacionado->save();

        return redirect('estacionado');
    }

    /**
     * Delete confirmation message by Ajaxis.
     *
     * @link      https://github.com/amranidev/ajaxis
     * @param    \Illuminate\Http\Request  $request
     * @return  String
     */
    public function DeleteMsg($id,Request $request)
    {
        $msg = Ajaxis::MtDeleting('Warning!!','Would you like to remove This?','/estacionado/'. $id . '/delete');

        if($request->ajax())
        {
            return $msg;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param    int $id
     * @return  \Illuminate\Http\Response
     */
    public function destroy($id)
    {
     	$estacionado = estacionado::findOrfail($id);
     	$estacionado->delete();
        return URL::to('estacionado');
    }
}
