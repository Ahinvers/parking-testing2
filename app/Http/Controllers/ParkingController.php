<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\App;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Parking;
use Amranidev\Ajaxis\Ajaxis;
use URL;

use App\Estado;


use App\Valet;


use App\Vehiculo;


/**
 * Class ParkingController.
 *
 * @author  The scaffold-interface created at 2020-08-31 07:49:40pm
 * @link  https://github.com/amranidev/scaffold-interface
 */
class ParkingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return  \Illuminate\Http\Response
     */
    public function index()
    {
        $title = 'Index - parking';
        $parkings = Parking::paginate(6);
        return view('parking.index',compact('parkings','title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return  \Illuminate\Http\Response
     */
    public function create()
    {
        $title = 'Create - parking';
        
        $estados = Estado::all()->pluck('Estado','id');
        
        $valets = Valet::all()->pluck('Rut','id');
        
        $vehiculos = Vehiculo::all()->pluck('Patente','id');
        
        return view('parking.create',compact('title','estados' , 'valets' , 'vehiculos'  ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param    \Illuminate\Http\Request  $request
     * @return  \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $parking = new Parking();

        
        $parking->Numero = $request->Numero;

        
        $parking->Lugar = $request->Lugar;

        
        $parking->Fecha = $request->Fecha;

        
        
        $parking->estado_id = $request->estado_id;

        
        $parking->valet_id = $request->valet_id;

        
        $parking->vehiculo_id = $request->vehiculo_id;

        
        $parking->save();

        $pusher = App::make('pusher');

        //default pusher notification.
        //by default channel=test-channel,event=test-event
        //Here is a pusher notification example when you create a new resource in storage.
        //you can modify anything you want or use it wherever.
        $pusher->trigger('test-channel',
                         'test-event',
                        ['message' => 'A new parking has been created !!']);

        return redirect('parking');
    }

    /**
     * Display the specified resource.
     *
     * @param    \Illuminate\Http\Request  $request
     * @param    int  $id
     * @return  \Illuminate\Http\Response
     */
    public function show($id,Request $request)
    {
        $title = 'Show - parking';

        if($request->ajax())
        {
            return URL::to('parking/'.$id);
        }

        $parking = Parking::findOrfail($id);
        return view('parking.show',compact('title','parking'));
    }

    /**
     * Show the form for editing the specified resource.
     * @param    \Illuminate\Http\Request  $request
     * @param    int  $id
     * @return  \Illuminate\Http\Response
     */
    public function edit($id,Request $request)
    {
        $title = 'Edit - parking';
        if($request->ajax())
        {
            return URL::to('parking/'. $id . '/edit');
        }

        
        $estados = Estado::all()->pluck('Estado','id');

        
        $valets = Valet::all()->pluck('Rut','id');

        
        $vehiculos = Vehiculo::all()->pluck('Patente','id');

        
        $parking = Parking::findOrfail($id);
        return view('parking.edit',compact('title','parking' ,'estados', 'valets', 'vehiculos' ) );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param    \Illuminate\Http\Request  $request
     * @param    int  $id
     * @return  \Illuminate\Http\Response
     */
    public function update($id,Request $request)
    {
        $parking = Parking::findOrfail($id);
    	
        $parking->Numero = $request->Numero;
        
        $parking->Lugar = $request->Lugar;
        
        $parking->Fecha = $request->Fecha;
        
        
        $parking->estado_id = $request->estado_id;

        
        $parking->valet_id = $request->valet_id;

        
        $parking->vehiculo_id = $request->vehiculo_id;

        
        $parking->save();

        return redirect('parking');
    }

    /**
     * Delete confirmation message by Ajaxis.
     *
     * @link      https://github.com/amranidev/ajaxis
     * @param    \Illuminate\Http\Request  $request
     * @return  String
     */
    public function DeleteMsg($id,Request $request)
    {
        $msg = Ajaxis::MtDeleting('Warning!!','Would you like to remove This?','/parking/'. $id . '/delete');

        if($request->ajax())
        {
            return $msg;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param    int $id
     * @return  \Illuminate\Http\Response
     */
    public function destroy($id)
    {
     	$parking = Parking::findOrfail($id);
     	$parking->delete();
        return URL::to('parking');
    }
}
