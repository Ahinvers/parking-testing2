<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\App;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Vehiculo;
use Amranidev\Ajaxis\Ajaxis;
use URL;

/**
 * Class VehiculoController.
 *
 * @author  The scaffold-interface created at 2020-08-31 07:42:35pm
 * @link  https://github.com/amranidev/scaffold-interface
 */
class VehiculoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return  \Illuminate\Http\Response
     */
    public function index()
    {
        $title = 'Index - vehiculo';
        $vehiculos = Vehiculo::paginate(6);
        return view('vehiculo.index',compact('vehiculos','title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return  \Illuminate\Http\Response
     */
    public function create()
    {
        $title = 'Create - vehiculo';
        
        return view('vehiculo.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param    \Illuminate\Http\Request  $request
     * @return  \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $vehiculo = new Vehiculo();

        
        $vehiculo->Patente = $request->Patente;

        
        $vehiculo->Marca = $request->Marca;

        
        $vehiculo->Modelo = $request->Modelo;

        
        $vehiculo->Comentario = $request->Comentario;

        
        $vehiculo->Nombre = $request->Nombre;

        
        $vehiculo->Contacto = $request->Contacto;

        
        
        $vehiculo->save();

        $pusher = App::make('pusher');

        //default pusher notification.
        //by default channel=test-channel,event=test-event
        //Here is a pusher notification example when you create a new resource in storage.
        //you can modify anything you want or use it wherever.
        $pusher->trigger('test-channel',
                         'test-event',
                        ['message' => 'A new vehiculo has been created !!']);

        return redirect('vehiculo');
    }

    /**
     * Display the specified resource.
     *
     * @param    \Illuminate\Http\Request  $request
     * @param    int  $id
     * @return  \Illuminate\Http\Response
     */
    public function show($id,Request $request)
    {
        $title = 'Show - vehiculo';

        if($request->ajax())
        {
            return URL::to('vehiculo/'.$id);
        }

        $vehiculo = Vehiculo::findOrfail($id);
        return view('vehiculo.show',compact('title','vehiculo'));
    }

    /**
     * Show the form for editing the specified resource.
     * @param    \Illuminate\Http\Request  $request
     * @param    int  $id
     * @return  \Illuminate\Http\Response
     */
    public function edit($id,Request $request)
    {
        $title = 'Edit - vehiculo';
        if($request->ajax())
        {
            return URL::to('vehiculo/'. $id . '/edit');
        }

        
        $vehiculo = Vehiculo::findOrfail($id);
        return view('vehiculo.edit',compact('title','vehiculo'  ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param    \Illuminate\Http\Request  $request
     * @param    int  $id
     * @return  \Illuminate\Http\Response
     */
    public function update($id,Request $request)
    {
        $vehiculo = Vehiculo::findOrfail($id);
    	
        $vehiculo->Patente = $request->Patente;
        
        $vehiculo->Marca = $request->Marca;
        
        $vehiculo->Modelo = $request->Modelo;
        
        $vehiculo->Comentario = $request->Comentario;
        
        $vehiculo->Nombre = $request->Nombre;
        
        $vehiculo->Contacto = $request->Contacto;
        
        
        $vehiculo->save();

        return redirect('vehiculo');
    }

    /**
     * Delete confirmation message by Ajaxis.
     *
     * @link      https://github.com/amranidev/ajaxis
     * @param    \Illuminate\Http\Request  $request
     * @return  String
     */
    public function DeleteMsg($id,Request $request)
    {
        $msg = Ajaxis::MtDeleting('Warning!!','Would you like to remove This?','/vehiculo/'. $id . '/delete');

        if($request->ajax())
        {
            return $msg;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param    int $id
     * @return  \Illuminate\Http\Response
     */
    public function destroy($id)
    {
     	$vehiculo = Vehiculo::findOrfail($id);
     	$vehiculo->delete();
        return URL::to('vehiculo');
    }
}
