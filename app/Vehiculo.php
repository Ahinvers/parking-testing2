<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Vehiculo.
 *
 * @author  The scaffold-interface created at 2020-08-31 07:42:35pm
 * @link  https://github.com/amranidev/scaffold-interface
 */
class Vehiculo extends Model
{
	
	
    public $timestamps = false;
    
    protected $table = 'vehiculos';

	
}
