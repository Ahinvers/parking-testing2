<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Estado.
 *
 * @author  The scaffold-interface created at 2020-08-31 07:38:53pm
 * @link  https://github.com/amranidev/scaffold-interface
 */
class Estado extends Model
{
	
	
    public $timestamps = false;
    
    protected $table = 'estados';

	
}
