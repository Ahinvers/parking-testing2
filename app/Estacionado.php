<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Parking.
 *
 * @author  The scaffold-interface created at 2020-08-31 07:49:40pm
 * @link  https://github.com/amranidev/scaffold-interface
 */
class Estacionado extends Model
{


    public $timestamps = false;

    protected $table = 'parkings';


	public function estado()
	{
		return $this->belongsTo('App\Estado','estado_id');
	}


	public function valet()
	{
		return $this->belongsTo('App\Valet','valet_id');
	}


	public function vehiculo()
	{
		return $this->belongsTo('App\Vehiculo','vehiculo_id');
	}


}
